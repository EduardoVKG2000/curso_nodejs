// Asignano la variable http que solicitas el modulo HTTP
var http = require('http');

//Creo una nueva variable (server) para que nos almacene un servidor con sus respuestas
//y el tipo de contenido que se enviara en este caso texto plano...
var server = http.createServer(function(request, response){
  response.writeHead(200, {'Content-Type': 'text/plain'});
  response.end('Servidor respondiendo a peticion...\n');
});

// Arrancando el createServer
// puertos 1337 y el 127.0.0.1

server.listen(1337, '127.0.0.1');
console.log('Servidor arrancado con exito...');

//prueba
